import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class DataService {
  private update$ = new Subject <any> ();
  Observable = this.update$.asObservable();

  // tslint:disable-next-line: variable-name
  constructor(private _client: HttpClient) { }

  async obtener() {
    // tslint:disable-next-line: prefer-const
    let locales = await this._client.get <any> (apiUrl);
    return this.update$.next(locales);
  }
}
