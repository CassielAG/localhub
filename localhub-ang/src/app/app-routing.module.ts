import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalleComponent } from './calle/calle.component';
import { FraccComponent } from './fracc/fracc.component';


const routes: Routes = [
    {path: '', redirectTo: 'fraccs', pathMatch: 'full'},
    {path: 'fraccs', component: FraccComponent},
    {path: 'calle', component: CalleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
