import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FraccComponent } from './fracc.component';

describe('FraccComponent', () => {
  let component: FraccComponent;
  let fixture: ComponentFixture<FraccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FraccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FraccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
