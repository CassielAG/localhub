import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calle',
  templateUrl: './calle.component.html',
  styleUrls: ['./calle.component.css']
})
export class CalleComponent implements OnInit {

  locales: any;

  // tslint:disable-next-line: variable-name
  constructor(private _dataService: DataService, private router: Router) {}

  ngOnInit(): void {
    this.actualizarTabla();
  }

  actualizarTabla() {
    this._dataService.Observable.subscribe(resultados => {
      this.locales = resultados;
  });

    this._dataService.obtener();
  }
}
