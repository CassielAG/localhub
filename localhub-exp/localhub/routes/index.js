var express = require('express');
var router = express.Router();

var Local = require('../models/locales');

/* GET home page. */
router.get('/', function(req, res, next) {
  //find es como un select y te trae todo
  Local.find().exec(function(error,locales){
    if(!error)
    {
      console.log(locales);
      res.render('index', { title: 'Todos los locales',coleccion:locales});
    }
  });
});

module.exports = router;
