let express = require("express");
let router = express.Router();

var localHub = require("../models/locales");

router.get("/", (req, res) => {
  localHub.find().exec((error, locales) => {
    if (!error) {
      res.status(200).json(locales);
    } else {
      res.status(500).json(error);
    }
  });
});

module.exports = router;